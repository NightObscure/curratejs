const axios = require('axios')
const fs = require('fs')

const service = 'https://currate.ru/api/'
const key = '6ffa404e7bba457f81a73d459ce0e520'
const interval = 6 // in hours

let count = null;
let from = null;
let to = null;

let forceupdate = false
let list = false
let help = false

for (let i = 2; i < process.argv.length; i++) {
	let arg = process.argv[i]

	if (arg.startsWith('-')) {
		switch(arg) {
			case '--forceupdate':
			case '-f':
				forceupdate = true
				break;
			case '--list':
			case '-l':
				list = true
				break;
			case '--help':
			case '-h':
				help = true
				break;
		}
	} else {
		if (count == null) count = Number(arg)
		else if (from == null) from = arg
		else if (to == null) to = arg
		else console.error(`unknown parameter with value '${arg}'`)
	}
}

process.argv.forEach((val, index) => {
	switch(index) {
		case 0:
		case 1:

			break;
		case 2:
			count = Number(val)
			break;
		case 3:
			from = val
			break;
		case 4:
			to = val
			break;
		default:
			switch(val) {
				case '--forceupdate':
				case '-f':
					forceupdate = true
					break;
			}
			break;
	}
})

if (help) {
	showHelp()
} else if (list) {
	showList()
} else if (count != null && from != null && to != null) {
	action()
}

async function getTable() {
	let table
	if (fs.existsSync(`${__dirname}/table.json`) && !forceupdate) {
		table = require(`${__dirname}/table.json`)

		if ((new Date().getTime()) - table.lastUpdate >= 1000 * 60 * 60 * interval) {
			table = await fetchTable()
			fs.writeFileSync(`${__dirname}/table.json`, JSON.stringify(table))
		}
	} else {
		table = await fetchTable()
		fs.writeFileSync(`${__dirname}/table.json`, JSON.stringify(table))
	}
	return table
}

async function action() {
	let table = await getTable()

	let coef
	if (table.curs[`${from}${to}`] != undefined) coef = table.curs[`${from}${to}`]
	else if (table.curs[`${to}${from}`] != undefined) coef = 1 / table.curs[`${to}${from}`]
	else {
		console.log(`cannot convert :( ${from} to ${to}`)
	}

	console.log('>' + (coef * count).toFixed(2))
}

async function showList() {
	let curdb = getCurdb()
	if (curdb) {
		let table = await getTable()
		for (let cur in curdb) {
			if (table.unics.indexOf(curdb[cur].key) != -1) console.log(curdb[cur].key.padEnd(3), curdb[cur].name.padEnd(25), curdb[cur].type.padEnd(30))
		}
	} else {
		console.error('unable to load curdb.json')
	}
}

async function showHelp() {
	console.log('Использование: call_script.sh <count> <from> <to>')
	console.log('')
	console.log('Параметры:')
	console.log('-f'.padEnd(2), '--forceupdate'.padEnd(15), 'Обязательно синхронизировать данные с серверов перед расчетом')
	console.log('-l'.padEnd(2), '--list'.padEnd(15),        'Получить список доступных валют')
	console.log('-h'.padEnd(2), '--help'.padEnd(15),        'Получить помощь')
}

function getCurdb() {
	if (fs.existsSync(`${__dirname}/curdb.json`)) {
		let curdb = require(`${__dirname}/curdb.json`)

		for (let i = 1; i < curdb.length; i++) {
			for (let j = i; j > 0; j--) {
				if (curdb[j].type == 'криптовалюта') {
					let temp = curdb[j]
					curdb[j] = curdb[j - 1]
					curdb[j - 1] = temp
				}
			}
		}
		return curdb
	} else {
		return null
	}
}

async function request(what, data) {
	data = data || {}

	console.log(`fetching ${what}...`)

	return new Promise((res, rej) => {
		let dat = ''
		for (let key in data) dat += `${key}=${data[key]}`
		if (!!dat) dat += '&'

		axios.get(`${service}?get=${what}&${dat}key=${key}`).then((response) => {
			if (response.data.status == 200) {
				res(response.data.data)
			} else {
				rej({code: response.data.status, message: response.data.message})
			}
		}, (err) => {
			console.error(`request error:\n${err}`)
			rej(err)
		})
	})
}

async function fetchTable() {
	let result = new Object()
	result.curs = new Object()

	let raw = await request('currency_list')
	let curs = await request('rates', {pairs: raw.join(',')})

	raw.forEach((val) => {
		result.curs[val] = curs[val]
	})
	result.lastUpdate = (new Date()).getTime()

	let ucurs = []
	for (let i = 0; i < raw.length; i++) {
		let key = raw[i]

		let a = key.substring(0, 3)
		let b = key.substring(3, 6)

		if (ucurs.indexOf(a) == -1) ucurs.push(a)
		if (ucurs.indexOf(b) == -1) ucurs.push(b)

	}
	result.unics = ucurs

	return result
}